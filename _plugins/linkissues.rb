Jekyll::Hooks.register([:pages, :posts], :post_render) do |post|
  post.output = post.output.gsub(/issue #([0-9]+)/, 'issue <a href="//tortoisegit.org/issue/\1" target="_blank">#\1</a>')
end
