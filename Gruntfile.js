'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        dirs: {
            dest: '_site',
            src: '_tortoisegit.org'
        },

        jekyll: {
            tortoisegitorg: {
                options: {
                    bundleExec: true,
                    config: '_config.yml,_tortoisegit.org.yml',
                    incremental: false
                }
            },
            downloadtortoisegitorg: {
                options: {
                    bundleExec: true,
                    config: '_config.yml,_download.tortoisegit.org.yml',
                    incremental: false
                }
            },
            downloadtortoisegitorglocal: {
                options: {
                    bundleExec: true,
                    config: '_config.yml,_download.tortoisegit.org.yml',
                    raw: 'layoutprefix: http://127.0.0.1:8000',
                    incremental: false
                }
            }
        },

        inlineImgSize: {
            tortoisegitorg: {
                files: [{
                    expand: true,
                    cwd: '<%= dirs.dest %>/tortoisegit.org',
                    src: ['**/*.html']
                }],
                options: {
                    root: '<%= dirs.dest %>/tortoisegit.org'
                }
            },
            downloadtortoisegitorg: {
                files: [{
                    expand: true,
                    cwd: '<%= dirs.dest %>/download.tortoisegit.org.org',
                    src: ['**/*.html']
                }],
                options: {
                    root: '<%= dirs.dest %>/download.tortoisegit.org.org'
                }
            },
        },

        htmlmin: {
            dist: {
                options: {
                    collapseBooleanAttributes: true,
                    collapseInlineTagWhitespace: false,
                    collapseWhitespace: true,
                    conservativeCollapse: false,
                    decodeEntities: true,
                    minifyCSS: {
                        //advanced: false,
                        compatibility: 'ie7',
                        keepSpecialComments: 0
                    },
                    minifyJS: true,
                    minifyURLs: false,
                    processConditionalComments: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    removeOptionalAttributes: true,
                    removeOptionalTags: false, // needed for sortable.js as this strips thead/tbody
                    removeRedundantAttributes: true,
                    removeScriptTypeAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    removeTagWhitespace: false,
                    sortAttributes: true,
                    sortClassName: true
                },
                expand: true,
                cwd: '<%= dirs.dest %>',
                dest: '<%= dirs.dest %>',
                src: ['**/*.html']
            }
        },

        concat: {
            css: {
                src: ['<%= dirs.src %>/css/fonts.css',
                      '<%= dirs.src %>/css/reset.css',
                      '<%= dirs.src %>/css/flags.css',
                      '<%= dirs.src %>/css/diricons.css',
                      '<%= dirs.src %>/css/style.css'
                ],
                dest: '<%= dirs.dest %>/tortoisegit.org/css/combined.min.css'
            }
        },

        cssmin: {
            minify: {
                options: {
                    compatibility: 'ie7',
                    keepSpecialComments: 0
                },
                files: {
                    '<%= concat.css.dest %>': '<%= concat.css.dest %>'
                }
            }
        },

        filerev: {
            css: {
                src: '<%= dirs.dest %>/tortoisegit.org/css/combined.min.css'
             },
            js: {
                src: [
                    '<%= dirs.dest %>/tortoisegit.org/download/sorttable.js'
                ]
            },
            images: {
                src: [
                    '<%= dirs.dest %>/tortoisegit.org/images/{flags,diricons}.png',
                    '<%= dirs.dest %>/tortoisegit.org/images/hero.{png,svgz}',
                    '<%= dirs.dest %>/tortoisegit.org/donate/*.{png,jpg}'
                ]
            },
            fonts: {
                src: '<%= dirs.dest %>/tortoisegit.org/fonts/**/*.{eot,svg,ttf,woff,woff2}'
            }
        },

        useminPrepare: {
            html: '<%= dirs.dest %>/index.html',
            options: {
                dest: '<%= dirs.dest %>',
                root: '<%= dirs.dest %>'
            }
        },

        usemin: {
            css: '<%= dirs.dest %>/tortoisegit.org/css/combined.min*.css',
            html: '<%= dirs.dest %>/**/*.html',
            options: {
                assetsDirs: ['<%= dirs.dest %>/tortoisegit.org/', '<%= dirs.dest %>/tortoisegit.org/css/', '<%= dirs.dest %>/tortoisegit.org/donate/'],
                patterns: {
  html: [
    [
      /<script.+src=['"](?:https?:)?\/\/(?:tortoisegit\.org|127\.0\.0\.1:8000)([^"']+)["']/gm,
      'Update the HTML to reference our concat/min/revved script files (https://tortoisegit.org)'
    ],
    [
      /<link[^\>]+href=['"](?:https?:)?\/\/(?:tortoisegit\.org|127\.0\.0\.1:8000)([^"' ]+)["']/gm,
      'Update the HTML with the new css filenames (https://tortoisegit.org)'
    ],
    [
      /<img[^\>]*[^\>\S]+src=['"](?:https?:)?\/\/(?:tortoisegit\.org|127\.0\.0\.1:8000)([^'"\)#]+)(#.+)?["']/gm,
      'Update the HTML with the new img filenames (https://tortoisegit.org)'
    ],


    [
      /<script.+src=['"]([^"']+)["']/gm,
      'Update the HTML to reference our concat/min/revved script files'
    ],
    [
      /<link[^\>]+href=['"]([^"' ]+)["']/gm,
      'Update the HTML with the new css filenames'
    ],
    [
      /<img[^\>]*[^\>\S]+src=['"]([^'"\)#]+)(#.+)?["']/gm,
      'Update the HTML with the new img filenames'
    ],
    [
      /<video[^\>]+src=['"]([^"']+)["']/gm,
      'Update the HTML with the new video filenames'
    ],
    [
      /<video[^\>]+poster=['"]([^"']+)["']/gm,
      'Update the HTML with the new poster filenames'
    ],
    [
      /<source[^\>]+src=['"]([^"']+)["']/gm,
      'Update the HTML with the new source filenames'
    ],
    [
      /data-main\s*=['"]([^"']+)['"]/gm,
      'Update the HTML with data-main tags',
      function (m) {
        return m.match(/\.js$/) ? m : m + '.js';
      },
      function (m) {
        return m.replace('.js', '');
      }
    ],
    [
      /data-(?!main).[^=]+=['"]([^'"]+)['"]/gm,
      'Update the HTML with data-* tags'
    ],
    [
      /url\(\s*['"]?([^"'\)]+)["']?\s*\)/gm,
      'Update the HTML with background imgs, case there is some inline style'
    ],
    [
      /<a[^\>]+href=['"]([^"']+)["']/gm,
      'Update the HTML with anchors images'
    ],
    [
      /<input[^\>]+src=['"]([^"']+)["']/gm,
      'Update the HTML with reference in input'
    ],
    [
      /<meta[^\>]+content=['"]([^"']+)["']/gm,
      'Update the HTML with the new img filenames in meta tags'
    ],
    [
      /<object[^\>]+data=['"]([^"']+)["']/gm,
      'Update the HTML with the new object filenames'
    ],
    [
      /<image[^\>]*[^\>\S]+xlink:href=['"]([^"'#]+)(#.+)?["']/gm,
      'Update the HTML with the new image filenames for svg xlink:href links'
    ],
    [
      /<image[^\>]*[^\>\S]+src=['"]([^'"\)#]+)(#.+)?["']/gm,
      'Update the HTML with the new image filenames for src links'
    ],
    [
      /<(?:img|source)[^\>]*[^\>\S]+srcset=['"]([^"'\s]+)\s*?(?:\s\d*?[w])?(?:\s\d*?[x])?\s*?["']/gm,
      'Update the HTML with the new image filenames for srcset links'
    ],
    [
      /<(?:use|image)[^\>]*[^\>\S]+xlink:href=['"]([^'"\)#]+)(#.+)?["']/gm,
      'Update the HTML within the <use> tag when referencing an external url with svg sprites as in svg4everybody'
    ]
  ],
  css: [
    [
      /(?:src=|url\(\s*)['"]?([^'"\)(\?|#)]+)['"]?\s*\)?/gm,
      'Update the CSS to reference our revved images'
    ]
  ]
                }
            }
        },

        connect: {
            tortoisegitorg: {
                options: {
                    port: 8000,
                    base: '<%= dirs.dest %>/tortoisegit.org/'
                }
            },
            downloadtortoisegitorg: {
                options: {
                    base: '<%= dirs.dest %>/download.tortoisegit.org/',
                    port: 8001
                }
           },
           options: {
                hostname: 'localhost',
                middleware: function(connect, options, middlewares) {
                    middlewares.unshift(function(req, res, next) {
                            if (/\.svgz$/.test(req.url)) {
                                res.setHeader('Content-Encoding', 'gzip');
                            }
                            if (/\.(eot|otf|ttf|woff2?)$/.test(req.url)) {
                                res.setHeader('Access-Control-Allow-Origin', '*');
                            }
                            next();
                        });
                        return middlewares;
                },
                livereload: 35729,
                open: true  // Automatically open the webpage in the default browser
            }
        },

        watch: {
            options: {
                livereload: '<%= connect.options.livereload %>'
            },
            dev: {
                files: ['_tortoisegit.org/**', '_download.tortoisegit.org/**', '.jshintrc', '_config.yml', 'Gruntfile.js', '_data/**', '_includes/**', '_layouts/**', '_shared/**', '_plugins/**'],
                tasks: 'dev'
            },
            build: {
                files: ['_tortoisegit.org/**', '_download.tortoisegit.org/**', '.jshintrc', '_config.yml', 'Gruntfile.js', '_data/**', '_includes/**', '_layouts/**', '_shared/**', '_plugins/**'],
                tasks: 'build'
            }
        },

        csslint: {
            options: {
                csslintrc: '.csslintrc'
            },
            src: '<%= dirs.src %>/css/*.css'
        },

        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            files: {
                src: ['Gruntfile.js']
            }
        },

        htmllint: {
            src: '<%= dirs.dest %>/**/*.html'
        },

        clean: {
            dist: '_site/'
        }
    });

    // Load any grunt plugins found in package.json.
    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});
    require('time-grunt')(grunt);

    grunt.registerTask('build', [
        'clean',
        'jekyll:tortoisegitorg',
        'jekyll:downloadtortoisegitorg',
        'inlineImgSize',
        'useminPrepare',
        'concat',
        'cssmin',
        'filerev',
        'usemin',
        'htmlmin'
    ]);

    grunt.registerTask('test', [
        'build',
        'csslint',
        'jshint',
        'htmllint'
    ]);

    grunt.registerTask('dev', [
        'jekyll:tortoisegitorg',
        'jekyll:downloadtortoisegitorglocal',
        'inlineImgSize',
        'useminPrepare',
        'concat',
        'filerev',
        'usemin'
    ]);

    grunt.registerTask('server', [
        'build',
        'connect',
        'watch:build'
    ]);

    grunt.registerTask('default', [
        'dev',
        'connect',
        'watch:dev'
    ]);
};
